package models

type HealthResponse struct {
  Version string `json:"version"`
  CurrentTime string `json:"currentTime"`
}

type FileResponse struct {
  FailureReason *string `json:"failureReason"`
	Name *string `json:"name"`
	Time *string `json:"time"`
}