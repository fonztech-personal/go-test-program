package routes

import (
  "fmt"
  "log"
  "time"
  "net/http"
  "math"
  "github.com/gofiber/fiber/v2"
  "crypto/sha256"
  "io/ioutil"
  "gotest/models"
)

func Health(app *fiber.App) {
  app.Get("/health", func(c *fiber.Ctx) error {
    hr := models.HealthResponse{
      Version: "1.0",
      CurrentTime: time.Now().String(),
    }
    return c.JSON(hr)
  })
}

func Test(app *fiber.App) {
  app.Get("/test", func(c *fiber.Ctx) error {
    // Create empty response
    fr := models.FileResponse{
      FailureReason: nil,
      Name: nil,
      Time: nil,
    }

    // Select random file
    url := ""
    index := -1
    if _, err := fmt.Sscanf(c.Query("index"), "%d", &index); err != nil {
      s := "Could not parse input index: " + err.Error()
      fr.FailureReason = &s
      return c.JSON(fr)
    }

    switch index {
    case 0:
      url = "https://raw.githubusercontent.com/BitDoctor/speed-test-file/master/1mb.txt"
      s := "1MB"
      fr.Name = &s
      
    case 1:
      url = "https://raw.githubusercontent.com/BitDoctor/speed-test-file/master/5mb.txt"
      s := "5MB"
      fr.Name = &s
      
    case 2:
      url = "https://raw.githubusercontent.com/BitDoctor/speed-test-file/master/10mb.txt"
      s := "10MB"
      fr.Name = &s
      
    case 3:
      url = "https://raw.githubusercontent.com/BitDoctor/speed-test-file/master/20mb.txt"
      s := "20MB"
      fr.Name = &s
    
    default:
      s := "Invalid index specified: " + c.Query("index")
      fr.FailureReason = &s
      return c.JSON(fr)
    }

    // Download file and check for errors
    log.Println("Downloading " + *fr.Name)
    curTime := float64(time.Now().Sub(time.Unix(0,0)).Milliseconds())

    resp, err := http.Get(url)
    if err != nil {
      s := "Error while downloading file: " + err.Error()
      fr.FailureReason = &s
      return c.JSON(fr)

    } else if resp.StatusCode != 200 {
      s := fmt.Sprintf("Status code was %d instead of 200", resp.StatusCode)
      fr.FailureReason = &s
      return c.JSON(fr)
      
    }
    defer resp.Body.Close()

    // Compute SHA256 of downloaded data
    data, err := ioutil.ReadAll(resp.Body)
    if err != nil {
      s := "Error while computing SHA256: " + err.Error()
      fr.FailureReason = &s
      return c.JSON(fr);
    }

    var hashes [][32]byte
    for i := 0; i < 1000; i++ {
      newHash := sha256.Sum256(data)
      hashes = append(hashes, newHash)
    }

    // Compute elapsed time
    {
      nowTime := float64(time.Now().Sub(time.Unix(0,0)).Milliseconds()) - curTime

      s := fmt.Sprint(int(math.Floor(nowTime / 1000.0))) + "." + fmt.Sprintf("%03d", int(nowTime) % 1000) + " s"
      fr.Time = &s
      log.Println("Elapsed time is " + *fr.Time)
    }

    // Return response
    return c.JSON(fr)
  })
}