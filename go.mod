module gotest

go 1.13

require (
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/gofiber/fiber/v2 v2.21.0
	github.com/klauspost/compress v1.13.6 // indirect
	golang.org/x/sys v0.0.0-20211031064116-611d5d643895 // indirect
)
