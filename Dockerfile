FROM golang:1.17-buster AS builder
WORKDIR /app
COPY go.* ./
RUN go mod download
COPY . ./
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -v -o gotest

FROM alpine:3.13
COPY --from=builder /app/gotest /app/gotest
CMD ["/app/gotest"]