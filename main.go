package main

import (
  "log"
  "github.com/gofiber/fiber/v2"
  "gotest/routes"
)

func main() {
  app := fiber.New()
  routes.Health(app)
  routes.Test(app)
  log.Fatal(app.Listen(":3000"))
}