#!/bin/bash
host=gotest:30000

echo "Executing load test for Go Test program"
index=0
while true; do

  echo "Calling endpoint with index ${index}"
  output=$(curl http://${host}/test?index=${index} > /dev/null 2>&1 &)
  code=$?

  if [[ "${code}" -ne "0" ]]; then
    echo "Load test was interrupted because 'curl' returned code ${code}"
    exit 1
  fi

  sleep 3

  index=$((index+1))
  if [[ "${index}" -eq "4" ]]; then
    index=0
  fi

done
